import json
import os
import pytest
import socket
from unittest.mock import patch

from tdameritrade_client.auth import TDAuthenticator

KEY_PATH = os.path.join(os.path.dirname(__file__), 'test_certs', 'key.pem')
CERT_PATH = os.path.join(os.path.dirname(__file__), 'test_certs', 'certificate.pem')
TOKEN_PATH = os.path.join(os.path.dirname(__file__), 'test_certs', 'auth-token.json')


@pytest.fixture
def create_test_ssl():
    # Also tests TDAuthenticator.create_ssl_cert
    try:
        os.makedirs(os.path.dirname(KEY_PATH))
    except FileExistsError:
        pass

    td_auth = TDAuthenticator(host='127.0.0.1', port=8080,
                              oauth_user_id='USERID',
                              token_path='./valid')
    td_auth._create_ssl_cert(CERT_PATH, KEY_PATH)


@pytest.fixture
def create_mock_token_json():
    try:
        os.makedirs(os.path.dirname(TOKEN_PATH))
    except FileExistsError:
        pass
    mock_token = {'access_token': 'access', 'refresh_token': 'refresh'}
    with open(TOKEN_PATH, 'w') as f:
        json.dump(mock_token, f)


class TestTDAuthenticator(object):
    def test_constructor_fails_with_invalid_token_path(self):
        with pytest.raises(ValueError):
            _ = TDAuthenticator(host='127.0.0.1', port=8080,
                                oauth_user_id='USERID',
                                token_path='invalid')

    def test__get_url_fails_with_wrong_type(self):
        with pytest.raises(NotImplementedError):
            td_auth = TDAuthenticator(host='127.0.0.1', port=8080,
                                      oauth_user_id='USERID',
                                      token_path='./valid')
            _ = td_auth._get_url('invalid')

    def test__get_url_auth_request(self):
        td_auth = TDAuthenticator(host='127.0.0.1', port=8080,
                                  oauth_user_id='USERID',
                                  token_path='./valid')
        url = td_auth._get_url('auth_request')
        assert url == 'https://auth.tdameritrade.com/auth?response_type=code&redirect_uri=http%3A//127.0.0.1%3A8080' \
                      '&client_id=USERID%40AMER.OAUTHAP'

    def test__get_url_token_request(self):
        td_auth = TDAuthenticator(host='127.0.0.1', port=8080,
                                  oauth_user_id='USERID',
                                  token_path='./valid')
        url = td_auth._get_url('token_request')
        assert url == 'https://api.tdameritrade.com/v1/oauth2/token'

    def test__create_ssl_cert(self, create_test_ssl):
        assert os.path.isfile(KEY_PATH)

    def test__start_server_context_closes(self, create_test_ssl):
        td_auth = TDAuthenticator(host='127.0.0.1', port=8080,
                                  oauth_user_id='USERID',
                                  token_path='./valid')
        with td_auth._start_server(KEY_PATH, CERT_PATH):
            pass
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        with pytest.raises(ConnectionRefusedError):
            s.connect(('127.0.0.1', 8080))
        s.close()

    def test__extract_code(self):
        response = b'GET /?code=lVP33%2B%2B5E HTTP/1.1\r\nHost:'
        td_auth = TDAuthenticator(host='127.0.0.1', port=8080,
                                  oauth_user_id='USERID',
                                  token_path='./valid')
        code, _ = td_auth._extract_code(response)
        assert code == 'lVP33++5E'

        _, success = td_auth._extract_code(b'')
        assert success is False

    @patch('tdameritrade_client.auth.TDAuthenticator.refresh_auth_token')
    def test_authenticate_will_refresh(self, mocked_refresh, create_mock_token_json):
        mocked_refresh.return_value = (True, 'success')
        td_auth = TDAuthenticator(host='127.0.0.1', port=8080,
                                  oauth_user_id='USERID',
                                  token_path=TOKEN_PATH)
        td_auth.authenticate()
        mocked_refresh.assert_called_with('refresh')

    @patch('tdameritrade_client.auth.TDAuthenticator.run_full_flow')
    def test_authenticate_will_run_full_flow(self, mocked_full_flow, create_mock_token_json):
        mocked_full_flow.return_value = 'success'
        mock_token = {'access_token': 'access', 'refresh_token': 'refresh'}
        with open(TOKEN_PATH, 'w') as f:
            json.dump(mock_token, f)
        td_auth = TDAuthenticator(host='127.0.0.1', port=8080,
                                  oauth_user_id='USERID',
                                  token_path='./bad_path')
        td_auth.authenticate()
        mocked_full_flow.assert_called_with()

import importlib
import os
from unittest.mock import patch

import pytest

import tdameritrade_client.utils.urls


class TestUrls(object):
    @patch('platform.system')
    @patch('environs.Env')
    def test_default_token_dir_windows(self, mocked_env, mocked_system):
        mocked_system.return_value = 'Windows'
        mocked_env.return_value = lambda env_var: env_var
        # Must reimport here for patch to work
        urls = importlib.reload(tdameritrade_client.utils.urls)
        assert urls.DEFAULT_TOKEN_DIR == os.path.join('HOMEDRIVE', 'HOMEPATH', '.tda_certs')

    @patch('platform.system')
    def test_default_token_dir_linux(self, mocked_system):
        mocked_system.return_value = 'Linux'
        # Must reimport here for patch to work
        urls = importlib.reload(tdameritrade_client.utils.urls)
        assert urls.DEFAULT_TOKEN_DIR == os.path.join(os.path.expanduser('~'), '.tda_certs')

    @patch('platform.system')
    def test_default_token_dir_darwin(self, mocked_system):
        mocked_system.return_value = 'Darwin'
        # Must reimport here for patch to work
        urls = importlib.reload(tdameritrade_client.utils.urls)
        assert urls.DEFAULT_TOKEN_DIR == os.path.join(os.path.expanduser('~'), '.tda_certs')

    @patch('platform.system')
    def test_default_token_dir_raises(self, mocked_system):
        mocked_system.return_value = 'not_supported'
        # Must reimport here for patch to work
        with pytest.raises(NotImplementedError):
            importlib.reload(tdameritrade_client.utils.urls)


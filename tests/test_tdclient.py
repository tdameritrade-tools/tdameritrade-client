from datetime import datetime
from unittest.mock import patch

import pytest

from tdameritrade_client.client import TDClient

CONFIG = {
    'acct_number': 123,
    'oauth_user_id': 'USERID',
    'redirect_uri': 'http://127.0.0.1:8080'
}


class TestTDClient(object):
    def test_from_dict(self):
        td_client = TDClient.from_dict({**CONFIG})
        assert td_client._oauth_user_id == CONFIG['oauth_user_id']

    def test__build_headers_fails_without_auth(self):
        td_client = TDClient.from_dict(CONFIG)
        with pytest.raises(AssertionError):
            td_client._build_header()

    def test__get_url_fails_with_wrong_type(self):
        with pytest.raises(NotImplementedError):
            td_client = TDClient.from_dict(CONFIG)
            td_client._get_url('invalid')

    def test__build_header(self):
        td_client = TDClient.from_dict(CONFIG)
        td_client.token = 'TEST'
        header = td_client._build_header()
        assert header['Authorization'] == 'Bearer TEST'

    def test_get_instrument_raises(self):
        td_client = TDClient.from_dict(CONFIG)
        td_client.token = 'TEST'
        with pytest.raises(NotImplementedError):
            td_client.get_instrument('ABC', 'unsupported')

    def test_get_movers_raises(self):
        td_client = TDClient.from_dict(CONFIG)
        td_client.token = 'TEST'
        with pytest.raises(AssertionError):
            td_client.get_movers(index='invalid', direction='up', change='percent')

        with pytest.raises(AssertionError):
            td_client.get_movers(index='$DJI', direction='invalid', change='value')

        with pytest.raises(AssertionError):
            td_client.get_movers(index='$DJI', direction='up', change='invalid')

    def test__get_url_positions(self):
        td_client = TDClient.from_dict(CONFIG)
        td_client.token = 'placeholder'
        url = td_client._get_url('positions', {'fields': 'positions'})
        assert url == 'https://api.tdameritrade.com/v1/accounts?fields=positions'

    def test__get_url_quote(self):
        td_client = TDClient.from_dict(CONFIG)
        url = td_client._get_url('get_quote', {'symbol': 'aaaa', 'projection': 'test'})
        assert url == 'https://api.tdameritrade.com/v1/marketdata/quotes?apikey=USERID&symbol=aaaa'

    def test__get_url_price_period(self):
        td_client = TDClient.from_dict(CONFIG)
        params = {
            'format': 'period',
            'symbol': 'AAAA',
            'periodType': 'day',
            'period': 3,
            'frequencyType': 'daily',
            'frequency': 1,
            'needExtendedHoursData': True
        }
        url = td_client._get_url('get_price_history', params)
        assert url == 'https://api.tdameritrade.com/v1/marketdata/AAAA/pricehistory?&periodType=day&period=3' \
                      '&frequencyType=daily&frequency=1&needExtendedHoursData=True'

    def test__get_url_price_date(self):
        td_client = TDClient.from_dict(CONFIG)
        params = {
            'format': 'date',
            'symbol': 'AAAA',
            'frequencyType': 'minute',
            'frequency': 1,
            'endDate': 123456789,
            'startDate': 123456788,
            'needExtendedHoursData': True
        }
        url = td_client._get_url('get_price_history', params)
        assert url == 'https://api.tdameritrade.com/v1/marketdata/AAAA/pricehistory?frequencyType=minute&frequency=1' \
                      '&endDate=123456789&startDate=123456788&needExtendedHoursData=True'

    def test__get_url_price_raises(self):
        td_client = TDClient.from_dict(CONFIG)
        params = {
            'format': 'invalid',
        }
        with pytest.raises(NotImplementedError):
            td_client._get_url('get_price_history', params)

    def test__get_url_movers(self):
        td_client = TDClient.from_dict(CONFIG)
        params = {
            'index': '$DJI',
            'direction': 'up',
            'change': 'value'
        }
        url = td_client._get_url('get_movers', params)
        assert url == 'https://api.tdameritrade.com/v1/marketdata/$DJI/movers?direction=up&change=value'

    def test__get_url_hours(self):
        td_client = TDClient.from_dict(CONFIG)
        params = {
            'markets': 'EQUITY',
            'date': datetime(2019, 1, 1).strftime('%Y-%m-%d')
        }
        url = td_client._get_url('get_hours', params)
        assert url == 'https://api.tdameritrade.com/v1/marketdata/hours?markets=EQUITY&date=2019-01-01'

    @patch('requests.get')
    def test_get_hours_raises(self, reply_mock):
        reply_mock.return_value.json.return_value = {'error': 'text'}
        td_client = TDClient.from_dict(CONFIG)
        td_client.token = 'placeholder'
        with pytest.raises(ValueError):
            td_client.get_hours(markets='EQUITY', date=datetime(2100, 1, 1))

    def test_get_hours_inputs_raise(self):
        td_client = TDClient.from_dict(CONFIG)
        td_client.token = 'placeholder'
        with pytest.raises(AssertionError):
            td_client.get_hours('invalid', datetime(2100, 1, 1))
        with pytest.raises(AssertionError):
            td_client.get_hours('EQUITY', datetime(2000, 1, 1))

    def test_get_transactions_raises(self):
        td_client = TDClient.from_dict(CONFIG)
        td_client.token = 'placeholder'
        with pytest.raises(AssertionError):
            td_client.get_transactions(datetime(2019, 1, 1), datetime(2019, 1, 2), type='invalid')
        with pytest.raises(AssertionError):
            td_client.get_transactions(datetime(2019, 2, 1), datetime(2019, 1, 2))
        with pytest.raises(AssertionError):
            td_client.get_transactions(datetime(2017, 2, 1), datetime(2019, 1, 2))

    def test__get_url_transactions(self):
        td_client = TDClient.from_dict(CONFIG)
        params = {
            'startDate': datetime(2019, 1, 1).strftime('%Y-%m-%d'),
            'endDate': datetime(2019, 2, 1).strftime('%Y-%m-%d'),
            'symbol': 'AAPL',
            'type': 'BUY_ONLY'
        }
        url = td_client._get_url('get_transactions', params)
        assert url == 'https://api.tdameritrade.com/v1/accounts/123/transactions?type=BUY_ONLY&symbol=AAPL' \
                      '&startDate=2019-01-01&endDate=2019-02-01'

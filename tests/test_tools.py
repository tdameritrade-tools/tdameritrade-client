from datetime import datetime

import pytest

from tdameritrade_client.utils.tools import validate_price_history_args, process_price_history

CONFIG = {
    'acct_number': 123,
    'oauth_user_id': 'USERID',
    'redirect_uri': 'http://127.0.0.1:8080'
}


class TestValidatePriceHistory(object):
    def test_validate_price_history_frequency_type_minute_raises(self):
        with pytest.raises(ValueError):
            validate_price_history_args(symbol='AAAA', frequency_type='minute', frequency=2)

    def test_validate_price_history_frequency_type_not_minute_raises(self):
        with pytest.raises(ValueError):
            validate_price_history_args(symbol='AAAA', frequency_type='daily', frequency=2)

    def test_validate_price_history_invalid_frequency_type_raises(self):
        with pytest.raises(ValueError):
            validate_price_history_args(symbol='AAAA', frequency_type='invalid', frequency=1)

    def test_validate_price_history_period_and_dates_raises(self):
        with pytest.raises(ValueError):
            validate_price_history_args(symbol='AAAA', period=1, start_date=datetime(2019, 1, 1))

    def test_validate_price_history_invalid_period_type_raises(self):
        with pytest.raises(ValueError):
            validate_price_history_args(symbol='AAAA', period_type='invalid')

    def test_validate_price_history_period_type_day_raises(self):
        with pytest.raises(ValueError):
            validate_price_history_args(symbol='AAAA', period_type='day', period=8, frequency_type='minute')

    def test_validate_price_history_period_type_day_frequency_raises(self):
        with pytest.raises(ValueError):
            validate_price_history_args(symbol='AAAA', period_type='day', period=3, frequency_type='invalid')

    def test_validate_price_history_period_type_month_raises(self):
        with pytest.raises(ValueError):
            validate_price_history_args(symbol='AAAA', period_type='month', period=4, frequency_type='daily')

    def test_validate_price_history_period_type_month_frequency_raises(self):
        with pytest.raises(ValueError):
            validate_price_history_args(symbol='AAAA', period_type='month', period=3, frequency_type='invalid')

    def test_validate_price_history_period_type_year_raises(self):
        with pytest.raises(ValueError):
            validate_price_history_args(symbol='AAAA', period_type='year', period=4, frequency_type='daily')

    def test_validate_price_history_period_type_year_frequency_raises(self):
        with pytest.raises(ValueError):
            validate_price_history_args(symbol='AAAA', period_type='year', period=3, frequency_type='invalid')

    def test_validate_price_history_period_type_ytd_raises(self):
        with pytest.raises(ValueError):
            validate_price_history_args(symbol='AAAA', period_type='ytd', period=4, frequency_type='daily')

    def test_validate_price_history_period_type_ytd_frequency_raises(self):
        with pytest.raises(ValueError):
            validate_price_history_args(symbol='AAAA', period_type='ytd', period=3, frequency_type='invalid')

    def test_validate_price_history_no_period_or_dates_raises(self):
        with pytest.raises(ValueError):
            validate_price_history_args(symbol='AAAA')

    def test_validate_price_history_invalid_dates_raises(self):
        with pytest.raises(ValueError):
            validate_price_history_args(symbol='AAAA', start_date=datetime(2019, 1, 1),
                                                   end_date=datetime(2017, 1, 1))


class TestProcessPriceHistory(object):
    def test_process_price_history_raises(self):
        response = {'error': 'bad request'}
        with pytest.raises(ValueError):
            process_price_history(response)

    def test_process_price_history_converts(self):
        response = {'candles': [
            {
                'open': 2,
                'datetime': 1556020800000
            }
        ]}
        processed_candles = process_price_history(response)
        assert processed_candles['candles'][0]['datetime'] == '2019-04-23_12-00-00'

    def test_process_price_history_removes(self):
        response = {'candles': [
            {
                'open': 2,
                'datetime': 1556020800000
            }
        ]}
        start_date = datetime(2019, 4, 23, 12, 1, 0)
        processed_candles = process_price_history(response, start_date)
        assert processed_candles['candles'] == []



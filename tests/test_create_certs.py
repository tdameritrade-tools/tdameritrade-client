from OpenSSL.crypto import dump_publickey, FILETYPE_PEM, PKey

from tdameritrade_client.utils.create_certs import get_serial_number, \
                                                   create_key_pair, \
                                                   create_certificate


class TestCreateCerts(object):
    def test_get_serial_number(self):
        version = get_serial_number()
        assert version >= 10000

    def test_create_key_pair(self):
        pkey = create_key_pair(2048)
        assert type(pkey) == PKey

    def test_create_certificate(self):
        pkey = create_key_pair(2048)
        cert = create_certificate(pkey)
        pubkey = dump_publickey(FILETYPE_PEM, cert.get_pubkey())
        expected = dump_publickey(FILETYPE_PEM, pkey)
        assert pubkey == expected


from tdameritrade_client import auth
from tdameritrade_client import client
from tdameritrade_client import utils

__version__ = '0.5.0'
__doc__ = \
    """
    python client for the TDA API
    """

Installation Instructions
=========================

System Dependencies
-------------------
Python 3.6 or 3.7.

Linux
-----
Create a virtual environment and install with::

    pip install tdameritrade-client

Windows
-------
Install Python 3 from here_. Create a virtual environment and install with::

    py -m pip install tdameritrade-client



.. _here: https://www.python.org/downloads/release/python-373/
